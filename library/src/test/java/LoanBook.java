import library.Book;
import library.Library;
import org.junit.Assert;
import org.junit.Test;

public class LoanBook {
    @Test
    public void loanBook1(){
        Book b = Library.addBook("Title",1932,"Author");
        Assert.assertTrue(Library.lent(b.getId(),"Name","Surname"));
    }

    @Test
    public void loanBook2(){
        Book b = Library.addBook("Title",1932,"Author");
        Assert.assertTrue(Library.lent(b.getId(),"Name","Surname"));
        Assert.assertTrue(!Library.lent(b.getId(),"Name2","Surname2"));
        }
}
