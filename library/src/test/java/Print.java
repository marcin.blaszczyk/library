import library.Book;
import library.Library;
import org.junit.Assert;
import org.junit.Test;

public class Print {
    @Test
    public void print() {
        Book b = Library.addBook("Title", 1932, "Author");
        Book b2 = Library.addBook("Title2", 1932, "Authorr");
        Book b3 = Library.addBook("Author", 1999, "Title");
        Assert.assertTrue(Library.print());
    }
}
