import library.Book;
import library.Library;
import org.junit.*;

public class AddBook {

    @Test
    public void addBook(){
        Book b =Library.addBook("Title",1932,"Author");
        Assert.assertTrue(Library.getLib().contains(b));
    }
}
