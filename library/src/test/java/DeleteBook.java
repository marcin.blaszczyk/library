import library.Book;
import library.Library;
import org.junit.Assert;
import org.junit.Test;

public class DeleteBook {

    @Test
    public void deleteBook(){
        Book b = Library.addBook("Title",1932,"Author");
        Library.delete(b.getId());
        Assert.assertTrue(!Library.getLib().contains(b));
    }
}
