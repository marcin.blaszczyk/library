import library.Book;
import library.Library;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;

public class SearchBook {


    @Test
    public void searchBook() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch(null,1932,null);
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook1() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch("Author",1932,null);
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook2() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch(null,1932,"Title");
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook3() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch(null,null,"Title");
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook4() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch("Author",null,null);
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook5() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch("Author",1932,"Title");
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
    @Test
    public void searchBook6() {
        Book b = Library.addBook("Title",1932,"Author");
        Book b2 = Library.addBook("Title2",1932,"Authorr");
        Book b3 = Library.addBook("Author",1999,"Title");
        LinkedList<Book> searched =Library.doSearch("Author",null,"Title");
        Assert.assertTrue(searched.contains(b));
        Assert.assertTrue(!searched.contains(b2));
        Assert.assertTrue(!searched.contains(b3));
    }
}
