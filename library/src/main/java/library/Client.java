/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.ArrayList;

/**
 *
 * @author Marcin
 */
public class Client {
    private String name;
    private String surname;
    private ArrayList<Book> booksLoaned;

    public Client(String name, String surname) {
        this.name = name;
        this.surname = surname;
        booksLoaned = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ArrayList<Book> getBooksLoaned() {
        return booksLoaned;
    }

    public void setBooksLoaned(ArrayList<Book> booksLoaned) {
        this.booksLoaned = booksLoaned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (name != null ? !name.equals(client.name) : client.name != null) return false;
        if (surname != null ? !surname.equals(client.surname) : client.surname != null) return false;
        return booksLoaned != null ? booksLoaned.equals(client.booksLoaned) : client.booksLoaned == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (booksLoaned != null ? booksLoaned.hashCode() : 0);
        return result;
    }
}
