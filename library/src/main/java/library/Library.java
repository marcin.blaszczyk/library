/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Marcin
 */
public class Library {
    private static LinkedList<Book> lib = new LinkedList<Book>();
    private static LinkedList<Client> clients = new LinkedList<Client>();
    private static Scanner s;

    public static LinkedList<Book> getLib() {
        return lib;
    }

    public static void setLib(LinkedList<Book> lib) {
        Library.lib = lib;
    }

    public static LinkedList<Client> getClients() {
        return clients;
    }

    public static void setClients(LinkedList<Client> clients) {
        Library.clients = clients;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        s = new Scanner(System.in);
        String option;
        while (true) {
            System.out.println("Library "
                    + "\n 1.Add book"
                    + "\n 2.List all books"
                    + "\n 3.Delete book" +
                    "\n 4.Search" +
                    "\n 5.Lent" +
                    "\n 6.Details");
            option = s.nextLine();
            switch (option) {
                case "1":
                    System.out.println("Provide title of book: ");
                    String title = s.nextLine();
                    System.out.println("Provide year of release: ");
                    Integer year = Integer.parseInt(s.nextLine());
                    System.out.println("Provide author of book: ");
                    String author = s.nextLine();
                    addBook(author,year,title);
                    break;
                case "2":
                    print();
                    break;
                case "3":
                    System.out.println("Provide id of book to delete: ");
                    int id = Integer.parseInt(s.nextLine());
                    delete(id);
                    break;
                case "4":
                    System.out.println("Provide title of book: ");
                    title = s.nextLine();
                    System.out.println("Provide year of release: ");
                    try{
                        year = Integer.parseInt(s.nextLine());
                    }catch(NumberFormatException ex){
                        System.out.println("Year as null");
                        year = null;
                    }
                    System.out.println("Provide author of book: ");
                    author = s.nextLine();
                    doSearch(title,year,author);
                case "5":
                    System.out.println("Provide name: ");
                    String name = s.nextLine();
                    System.out.println("Provide sourname");
                    String surname = s.nextLine();
                    System.out.println("Provide id of book to lent: ");
                    id = Integer.parseInt(s.nextLine());
                    lent(id,name,surname);
                    break;
                case "6":
                    System.out.println("Provide id of book which details you want to see: ");
                    id = Integer.parseInt(s.nextLine());
                    detais(id);
                    break;
                default:
                    return;
            }
        }

    }

    public static boolean detais(int id) {
        Book b = lib.stream().filter(x -> x.getId()==id).findFirst().get();
        System.out.println("Title: "+b.getTitle() +", author: " + b.getAuthor() + ", year: "+b.getYear() + ",available: "+!b.isLoaned());
        if(b.isLoaned()){
            Client c =clients.stream().filter( x->x.getBooksLoaned().contains(b)).findFirst().get();
            System.out.println("Loaned by: "+c.getName()+ " " +c.getSurname());

        }
        return true;
    }

    public static void delete(int id) {
        try {
            Book toDelete = lib.stream().filter(x -> x.getId() == id).findFirst().get();
            if (toDelete.isLoaned()) {
                System.out.println("Book is actually loaned. You can delete it now.");
            } else {
                lib.remove(toDelete);
            }
        } catch (NoSuchElementException ex) {
            System.out.println("There is no book with provided id");
        }
    }

    public static boolean lent(int id, String name, String surname){
        try {
            Book toLent = lib.stream().filter(x -> x.getId() == id).findFirst().get();
            if (toLent.isLoaned()) {
                System.out.println("Book is actually loaned.");
                return false;
            } else {
                toLent.setLoaned(true);
                Client client = clients.stream().filter(x-> x.getName().equals(name) && x.getSurname().equals(surname)).findFirst().orElse(new Client(name,surname));
                if(!clients.contains(client)){
                    clients.add(client);
                }
                client.getBooksLoaned().add(toLent);
                return true;
            }
        } catch (NoSuchElementException ex) {
            System.out.println("There is no book with provided id");
            return false;
        }
    }

    public static LinkedList<Book> doSearch(String title, Integer year, String author){
        String titl = title;
        String auth = author;
        if(title == null){
            titl="";
        }
        if(auth==null){
            auth="";
        }
        return search(titl,year,auth);
    }


    private static LinkedList<Book> search(String title, Integer year, String author) {
        LinkedList<Book> s = new LinkedList<>();
        if (title.equals("")&& year!=null && (!author.equals(""))){
            //year and author
            lib.stream().filter(x -> x.getYear().equals(year) && x.getAuthor().equals(author))
                    .forEach(x ->s.add(x));
        }else if( author.equals("")&& year!=null && (!title.equals(""))){
            //title and year
            lib.stream().filter(x -> x.getYear().equals(year) && x.getTitle().equals(title))
                    .forEach(x -> s.add(x));
        }else if(year==null && (title!=null && !title.equals("")) && (!author.equals(""))){
            //title and author
            lib.stream().filter(x -> x.getTitle().equals(title) && x.getAuthor().equals(author))
                    .forEach(x ->s.add(x));
        }else if (( !title.equals(""))&& year!=null && (!author.equals(""))){
            //all
            lib.stream().filter(x -> x.getYear().equals(year) && x.getAuthor().equals(author) && x.getTitle().equals(title))
                    .forEach(x ->s.add(x));
        }else if  ((title.equals(""))&& year==null && (!author.equals(""))){
            //author
            lib.stream().filter(x -> x.getAuthor().equals(author))
                    .forEach(x ->s.add(x));
        }else if ( year!=null && (title.equals("")) && (author.equals(""))){
            //year
            lib.stream().filter(x -> x.getYear().equals(year))
                    .forEach(x ->s.add(x));
        }else if ((!title.equals(""))&& year==null && (author.equals(""))){
            //title
            lib.stream().filter(x -> x.getTitle().equals(title))
                    .forEach(x ->s.add(x));
        }
        s.stream().forEach(x -> System.out.println(x));
        return s;
    }

    public static Boolean print() {
        LinkedList<Book> printed = new LinkedList<>();
        long count;
        for(Book b : lib){
            count = printed.stream().filter(x-> x.getAuthor().equals(b.getAuthor()) && x.getTitle().equals(b.getTitle())
                    && x.getYear().equals(b.getYear())).count();
            if(count==0){
               printed.add(b);
               long all = lib.stream().filter(x -> x.getYear().equals(b.getYear()) && x.getTitle().equals(b.getTitle())&&
                       x.getAuthor().equals(b.getAuthor())).count();
               long available = lib.stream().filter(x -> x.getYear().equals(b.getYear()) && x.getTitle().equals(b.getTitle())&&
                       x.getAuthor().equals(b.getAuthor()) && x.isLoaned()==false).count();
                System.out.println("Author: "+b.getAuthor()+ ", title: "+b.getTitle()+ ", year: "+b.getYear()+ ", all:"+all+", available:"
                +available);

            }
        }
        return true;
    }

    public static Book addBook(String author, int year, String title){
        int id = (lib.stream().mapToInt(x -> x.getId()).max().orElse(0)) + 1;
        Book b = new Book(id, title, year, author);
        lib.add(b);
        return b;
    }

}
