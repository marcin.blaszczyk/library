/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.Objects;

/**
 *
 * @author Marcin
 */
public class Book {

    private int id;
    private String title;
    private Integer year;
    private String author;
    private boolean loaned;

    public Book(int id, String title, Integer year, String author, boolean loaned) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.author = author;
        this.loaned = loaned;
    }

    public Book(int id, String title, Integer year, String author) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.author = author;
        this.loaned = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isLoaned() {
        return loaned;
    }

    public void setLoaned(boolean loaned) {
        this.loaned = loaned;
    }

    @Override
    public String toString() {
        return "Book{" + "id=" + id + ", title=" + title + ", year=" + year + ", author=" + author + ", loaned=" + loaned + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (loaned != book.loaned) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (year != null ? !year.equals(book.year) : book.year != null) return false;
        return author != null ? author.equals(book.author) : book.author == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (loaned ? 1 : 0);
        return result;
    }
}
